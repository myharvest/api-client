import es6Promise from 'es6-promise';
import RESTClient from 'rest-client';
es6Promise.polyfill()

import esc from 'jquery-param';

const LOCALSTORAGE_KEY = 'myharvest-api-client';

class myharvestAPIClient extends RESTClient {
  constructor(baseUrl) {
    super(baseUrl, {
      AUTH_PATH: '/auth/password/',
      LOGOUT_PATH: '/auth/logout/'
    })

    this.register = this._resource({
      route: 'registration/password'
    })

    this.forgotPassword = this._resource({
      route: 'auth/forgot-password'
    })

    this.resetPassword = this._resource({
      route: 'auth/reset-password'
    })

    this.tags = this._resource({
      route: 'tags'
    })

    this.products = this._resource({
      route: 'products'
    })

    this.producers = this._resource({
      route: 'producers'
    })

    this.orders = this._resource({
      route: 'orders'
    })

    this.webOrders = this._resource({
      route: 'web-orders'
    })
  }
}

export default myharvestAPIClient;

export { LOCALSTORAGE_KEY }