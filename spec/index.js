import assert from 'assert';
import myharvestAPIClient from '../src';
import nock from 'nock';
import fetchEverywhere from 'fetch-everywhere';
import {LocalStorage} from 'node-localstorage';
import {LOCALSTORAGE_KEY} from '../src';

var BASE = 'http://localhost:6767';
global.localStorage = new LocalStorage('./.tmp');

describe('basic', () => {
  it('should export properly', () => {
    var client = new myharvestAPIClient('http://127.0.0.1')
  })
})

describe('auth', () => {
  it('should call correct logout url', () => {
    var client = new myharvestAPIClient(BASE)

    nock(BASE)
      .post('/auth/logout/')
      .reply(200, {})

    client.token = 'token'
    client.user = 1
    return client.logOut()
  })
})