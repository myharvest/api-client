'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOCALSTORAGE_KEY = undefined;

var _es6Promise = require('es6-promise');

var _es6Promise2 = _interopRequireDefault(_es6Promise);

var _restClient = require('rest-client');

var _restClient2 = _interopRequireDefault(_restClient);

var _jqueryParam = require('jquery-param');

var _jqueryParam2 = _interopRequireDefault(_jqueryParam);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

_es6Promise2.default.polyfill();

var LOCALSTORAGE_KEY = 'myharvest-api-client';

var myharvestAPIClient = function (_RESTClient) {
  _inherits(myharvestAPIClient, _RESTClient);

  function myharvestAPIClient(baseUrl) {
    _classCallCheck(this, myharvestAPIClient);

    var _this = _possibleConstructorReturn(this, (myharvestAPIClient.__proto__ || Object.getPrototypeOf(myharvestAPIClient)).call(this, baseUrl, {
      AUTH_PATH: '/auth/password/',
      LOGOUT_PATH: '/auth/logout/'
    }));

    _this.register = _this._resource({
      route: 'registration/password'
    });

    _this.forgotPassword = _this._resource({
      route: 'auth/forgot-password'
    });

    _this.resetPassword = _this._resource({
      route: 'auth/reset-password'
    });

    _this.tags = _this._resource({
      route: 'tags'
    });

    _this.products = _this._resource({
      route: 'products'
    });

    _this.producers = _this._resource({
      route: 'producers'
    });

    _this.orders = _this._resource({
      route: 'orders'
    });

    _this.webOrders = _this._resource({
      route: 'web-orders'
    });

    return _this;
  }

  return myharvestAPIClient;
}(_restClient2.default);

exports.default = myharvestAPIClient;
exports.LOCALSTORAGE_KEY = LOCALSTORAGE_KEY;
