# Introduction

This API client will be used by the webapp and the mobile app to communicate with the server.

# Development

1. Clone the repository
2. Run `npm install`

# Testing

Testing can be run via `npm test`.  Tests are contained in the `spec` directory.

# Releasing

A new release can be generated via `npm run release`.  This will do the following:

- Ask you what you want to tag the release as
- Update `package.json`
- Build the client
- Create a new commit containing the updated build & `package.json`
- Tag the commit with the release number
- Push the tags & commit to master

# Usage in projects

Install using `npm install git+https://gitlab.com/my-harvest/api-client.git`

Usage:

```
import inConcertAPIClient from 'inconcert-api-client'

const client = new inConcertAPIClient('https://api.redpinemusic.com')

client.ready.then(() => {
  client.logIn(username, password).then(() => {
    // logged in
  }).catch((err) => {
    // log in failed
  })

  client.campaigns.get(1).then((campaign) => {
    // loads campaign data for campaign with id #1
  })
})
```

## Methods

### `client.ready`

This is a special promise that is resolved once session data is loaded.  All client code should be wrapped in `client.ready.then(() => { .. })`.

### `client.logIn(username, password)`

Logs a user in.  Returns a promise which is resolved when the user is logged in, or rejected when login fails.

### `client.facebookLogIn({ code: 'code', redirect_uri: 'redirectUri' })`

Logs a user in via Facebook OAuth.  Returns a promise which is resolved when the user is logged in, or rejected when login fails.

### REST Resources

The client implements a number of REST resources which have the same methods:

- Get a resource by ID: `client.resource.get(id)`
- Query a list of resources: `client.resource.list(params)`
- Update a resource by ID: `client.resource.update(id, update)`
- Delete a resource by ID: `client.resource.delete(id)`

Each resource returns a promise, with the relevant data.

Currently implemented REST resources:



# BYOF (Bring your own fetch)

This client relies on `fetch` being available.  This will need to be polyfilled in the browser (use [Github's implementation](https://github.com/github/fetch)) and included as `global.fetch = require('node-fetch')` in mobile projects (assuming the use of [`node-fetch](https://github.com/bitinn/node-fetch))